const express = require('express')
const app = express()
const port =  process.env.PORT || 3000 
const path = require('path')
const cors = require ('cors')
const router = require('./routes/route')

const  bodyParser = require('body-parser')
const passport = require('passport')





app.set('view engine','ejs')
app.set("views", path.join(__dirname, "views"))

app.use(cors())
app.use(express.static(__dirname + '/public'));


//app.use (express.json())
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.query())

app.use('/',router)
app.use(passport.initialize())
require('./middlewares/passport')
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})


module.exports = app 