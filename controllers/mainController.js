const {User,Tweet} = require ('../models')
const bcrypt= require('bcrypt')
const jwt = require('jsonwebtoken')
module.exports = {
    register : (req,res) =>{
        res.render('register')
    },
    doRegister : (req,res)=>{
        const {username, password } = req.body
        const passwordEncrypted = bcrypt.hashSync(password, 10)
        User.create({
            username:username,
            password:passwordEncrypted
        }).then (user =>{
            res.send ({
                success: true ,
                message : "User created",
                Username: username,
                password:password
            })
        }).catch (err=>{
            res.send ({
                success : false ,
                message :"user failed ",
                error : err
            })          
        })
    },
    doLogin :(req,res)=>{
        const {username,password}= req.body
        User.findOne({
            where :{
                username:username
            }
        }).then(user=>{
            if (!user){
                return res.status(401).send({
                    success:false,
                    message:"user is not found ",
                })
            }
            if (!bcrypt.compareSync(password,user.password)){
                return res.status(401).send({
                    success:false,
                    message:"incorect password "

                })
            }
            const payload = {
                id :user.id,
                username:user.username
            }
            console.log (`payload ${payload.id}`)
            console.log (`payload ${payload.username}`)

            const token = jwt.sign(payload, "secret", {expiresIn:"1d"})
            
            console.log (token)

            res.status(200).send({
                success:true,
                message:" logged in",
                token : "Bearer " + token

            })

        })
    },
    login : (req,res) =>{
        res.render('login')
    },
    getDashboard :(req,res) => {
        Tweet.findAll({
            where :{
                UserId :req.user.id
            }
        }).then (tweet=>{
            console.log(tweet)
            return res.status(200).send({
                success:true,
                user : {
                    id:req.user.id,
                    user:req.user.username,
                    tweets:tweet
                }
            })
        })
    },
    deleteDashboard :(req,res) => {
        const id = req.params.id
        console.log (id)
        Tweet.destroy({
            where:{
                id:id
            }
        }).then(()=>{
            return res.status(200).send({
                success:true,
            })
        })
    },
    updateDashboard :(req,res) => {
        const {text}= req.body
        const id = req.params.id
        console.log (text)
        console.log (id)
        Tweet.update(
           { text:text},
           {where : {id:id}}
        ).then (()=>{
        return res.status(200).send({
            success:true,
        })
        })
    },
    postDashboard :(req,res) => {
        const {text} =  req.body
        const id = req.user.id
        Tweet.create({
            text:text,
            UserId: id
        }).then (()=>{
            return res.status(200).send ({
                success: true ,
                message : "Text Added",
            })
        }).catch (err=>{
            return res.status(401).send  ({
                success : false ,
                error : err
            })          
        })
        
        
    },
}