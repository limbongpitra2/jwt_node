const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

const opts = {}
const {User} = require('../models')
const passport = require('passport')

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';
passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    User.findOne(
        {
            where : {id: jwt_payload.id}
        }).then(user =>{
            if (user) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        })
}));