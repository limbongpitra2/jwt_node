const router = require('express').Router();

const passport = require('passport');

const userController = require('../controllers/mainController')

const auth = passport.authenticate('jwt',{session:false})

router.get('/login', userController.login )
router.post('/login', userController.doLogin )

router.get('/register', userController.register )
router.post('/register', userController.doRegister )


router.get ('/home', auth ,userController.getDashboard)
router.post ('/home', auth ,userController.postDashboard)
router.delete ('/home/:id', auth ,userController.deleteDashboard)
router.put ('/home/:id', auth ,userController.updateDashboard)


module.exports = router 

